import { constantRoutes } from '@/router'
import Layout from '@/layout'
export function generaMenu(routes, data) {
    let name, split;
    data.forEach(item => {
        name = item.id + "_key";
        if (item.type === 'C') {
            split = item.path.split("/");
            name = split[split.length - 1];
        }
        const menu = {
            path: item.type === 'M' ? item.path : item.path.indexOf("http") > -1 ? item.path : name.toLocaleLowerCase(),
            component: item.type === 'M' ? Layout : (resolve) => require([`@/views${item.path}`], resolve),
            children: [],
            name: name,
            hidden: item.visible == 1 ? true : false,
            meta: { title: item.name, icon: item.icon }
        }
        if (item.children) {
            generaMenu(menu.children, item.children)
        }
        routes.push(menu);
    });
}

const state = {
    routes: [],
    addRoutes: []
}

const mutations = {
    SET_ROUTES: (state, routes) => {
        state.addRoutes = routes
        state.routes = constantRoutes.concat(routes)
    }
}

const actions = {
    generateRoutes({ commit }, menus) {
        return new Promise(resolve => {
            let accessedRoutes = []

            if (menus == null || menus == undefined) {
                this.$message({
                    message: '菜单数据加载异常',
                    type: 0
                })
            } else {
                generaMenu(accessedRoutes, menus);
                //accessedRoutes.push({ path: '*', redirect: '/404', hidden: true });
            }
            commit('SET_ROUTES', accessedRoutes)
            resolve(accessedRoutes)
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}