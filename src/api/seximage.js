import request from '@/utils/request'

export function getImageType() {
    return request({
        url: '/mobileApi/getImageType',
        method: 'get'
    })
};

export const getImageGroupsByType = (params) => {
    return request({
        url: "/mobileApi/getImageGroupsByType",
        method: "get",
        params: { type: params.type, current: params.currentPage, size: params.pageSize }
    });
};

getImagesByParentId

export function getImagesByParentId(id) {
    return request({
        url: '/mobileApi/getImagesByParentId/' + id,
        method: 'get'
    })
};