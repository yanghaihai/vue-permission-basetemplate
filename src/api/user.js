import request from '@/utils/request'

export function login(data) {
    return request({
        url: '/authentication',
        method: 'post',
        data: data
    })
};

export const getKaptcha = () => {
    return request({
        url: "/kaptcha",
        method: "get"
    });
};

export function getInfo(token) {
    return request({
        url: '/userInfo',
        method: 'get',
        params: { token }
    })
};