import request from '@/utils/request'

//角色列表接口
export const getRoleList = (roleQueryForm) => {
    return request({
        url: '/system/role/list',
        method: 'post',
        data: roleQueryForm
    })
}

//新增角色
export const insertRole = (roleForm) => {
    return request({
        url: '/system/role/save',
        method: 'post',
        data: roleForm
    })
}

//更新角色
export const updateRole = (roleForm) => {
    return request({
        url: '/system/role/update',
        method: 'post',
        data: roleForm
    })
}

//删除角色
export const delRole = (roleId) => {
    return request({
        url: '/system/role/del/' + roleId,
        method: 'get'
    })
}

//部门树列表接口
export const getDeptTree = (deptQueryForm) => {
    return request({
        url: '/system/dept/tree',
        method: 'post',
        data: deptQueryForm
    })
}

//新增部门
export const insertDept = (deptForm) => {
    return request({
        url: '/system/dept/save',
        method: 'post',
        data: deptForm
    })
}

//更新部门
export const updateDept = (deptForm) => {
    return request({
        url: '/system/dept/update',
        method: 'post',
        data: deptForm
    })
}

//删除部门
export const delDept = (deptId) => {
    return request({
        url: '/system/dept/del/' + deptId,
        method: 'get'
    })
}

//菜单树列表接口
export const getMenuTree = (menuQueryForm) => {
    return request({
        url: '/system/menu/tree',
        method: 'post',
        data: menuQueryForm
    })
}

//新增菜单
export const insertMenu = (menuForm) => {
    return request({
        url: '/system/menu/save',
        method: 'post',
        data: menuForm
    })
}

//更新菜单
export const updateMenu = (menuForm) => {
    return request({
        url: '/system/menu/update',
        method: 'post',
        data: menuForm
    })
}

//删除菜单
export const delMenu = (menuId) => {
    return request({
        url: '/system/menu/del/' + menuId,
        method: 'get'
    })
}

//岗位列表接口
export const getPostList = (postQueryForm) => {
    return request({
        url: '/system/post/list',
        method: 'post',
        data: postQueryForm
    })
}

//新增岗位
export const insertPost = (postForm) => {
    return request({
        url: '/system/post/save',
        method: 'post',
        data: postForm
    })
}

//更新岗位
export const updatePost = (postForm) => {
    return request({
        url: '/system/post/update',
        method: 'post',
        data: postForm
    })
}

//删除岗位
export const delPost = (postId) => {
    return request({
        url: '/system/post/del/' + postId,
        method: 'get'
    })
}

//用户列表接口
export const getUserList = (userQueryForm) => {
    return request({
        url: '/system/user/list',
        method: 'post',
        data: userQueryForm
    })
}

//新增用户
export const insertUser = (userForm) => {
    return request({
        url: '/system/user/save',
        method: 'post',
        data: userForm
    })
}

//更新用户
export const updateUser = (userForm) => {
    return request({
        url: '/system/user/update',
        method: 'post',
        data: userForm
    })
}

//删除用户
export const delUser = (userId) => {
    return request({
        url: '/system/user/del/' + userId,
        method: 'get'
    })
}

//字典类型列表接口
export const getDictTypeList = (dictTypeQueryForm) => {
    return request({
        url: '/system/dictType/list',
        method: 'post',
        data: dictTypeQueryForm
    })
}

//新增字典类型
export const insertDictType = (dictTypeForm) => {
    return request({
        url: '/system/dictType/save',
        method: 'post',
        data: dictTypeForm
    })
}

//更新字典类型
export const updateDictType = (dictTypeForm) => {
    return request({
        url: '/system/dictType/update',
        method: 'post',
        data: dictTypeForm
    })
}

//删除字典类型
export const delDictType = (dictTypeId) => {
    return request({
        url: '/system/dictType/del/' + dictTypeId,
        method: 'get'
    })
}


//字典数据列表接口
export const getDictDataList = (dictDataQueryForm) => {
    return request({
        url: '/system/dictData/list',
        method: 'post',
        data: dictDataQueryForm
    })
}

//新增字典数据
export const insertDictData = (dictDataForm) => {
    return request({
        url: '/system/dictData/save',
        method: 'post',
        data: dictDataForm
    })
}

//更新字典数据
export const updateDictData = (dictDataForm) => {
    return request({
        url: '/system/dictData/update',
        method: 'post',
        data: dictDataForm
    })
}

//删除字典数据
export const delDictData = (dictDataId) => {
    return request({
        url: '/system/dictData/del/' + dictDataId,
        method: 'get'
    })
}